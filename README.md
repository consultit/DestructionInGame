Makefiles have extension .mk because of sis controll

Dependencies

Following packages are required to compile the application.

We also provide versions of packages used to create and test our implementation.

Package versions:

+ libbullet-dev 2.83.7+dfsg-5
+ libirrlicht-dev 1.8.4+dfsg1-1
+ libcgal-dev 4.9-1build2
+ voro++-dev 0.4.6+dfsg1-2
+ HACD is bundled with the application1

A C++ compiler capable of compiling the C++14 standard is also needed,
we have used `GCC version 6.3.0-2ubuntu1` with flags: 

```
-std=c++14 -O2 -frounding-math.
```

To compile the application use make from this directory.

See: [Efficient simulation of environment destruction in games](https://is.cuni.cz/webapps/zzp/detail/181427/35350753/?q=%7B%22______searchform___search%22%3A%22efficient+simulation%22%2C%22______searchform___butsearch%22%3A%22Vyhledat%22%2C%22______facetform___facets___workType%22%3A%5B%22BP%22%5D%2C%22______facetform___facets___defenseYear%22%3A%5B%222017%22%5D%2C%22______facetform___facets___lang%22%3A%5B%22ENG%22%5D%2C%22PNzzpSearchListbasic%22%3A1%7D&lang=en)
